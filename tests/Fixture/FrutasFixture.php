<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FrutasFixture
 */
class FrutasFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id_fruta' => 1,
                'classificacao_id' => 1,
                'nome' => 'Lorem ipsum dolor sit amet',
                'fresca' => 1,
                'qtd_disponivel' => 1,
                'preco' => 1.5,
                'created' => 1675883065,
                'modified' => 1675883065,
            ],
        ];
        parent::init();
    }
}
