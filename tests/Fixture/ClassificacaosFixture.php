<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClassificacaosFixture
 */
class ClassificacaosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id_classificacao' => 1,
                'classificacao' => 'Lorem ipsum dolor sit amet',
                'created' => 1675883051,
                'modified' => 1675883051,
            ],
        ];
        parent::init();
    }
}
