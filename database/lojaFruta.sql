create table classificacaos(
    id_classificacao serial primary key,
    classificacao varChar not null,
    created timestamp,
    modified timestamp
);

create table frutas(
    id_fruta serial primary key,
    classificacao_id integer not null,
    nome varChar not null,
    fresca boolean not null,
    qtd_disponivel integer not null,
    preco numeric not null,
    created timestamp,
    modified timestamp
);

alter table frutas add foreign key(classificacao_id) references classificacaos(id_classificacao);

create table users(
    id_user serial primary key,
    nome varChar not null,
    password text not null,
    documento varChar not null,
    isAdm boolean not null,
    created timestamp,
    modified timestamp
);       

create table vendas(
    id_venda serial primary key,
    fruta_id integer not null,
    user_id integer not null,
    qtd_vendida integer not null,
    desconto numeric not null,
    created timestamp,
    modified timestamp
);

alter table vendas add foreign key(fruta_id) references frutas(id_fruta);
alter table vendas add foreign key(user_id) references users(id_user);