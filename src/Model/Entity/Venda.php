<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Venda Entity
 *
 * @property int $id_venda
 * @property int $fruta_id
 * @property int $user_id
 * @property int $qtd_vendida
 * @property string $desconto
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Fruta $fruta
 * @property \App\Model\Entity\User $user
 */
class Venda extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'fruta_id' => true,
        'user_id' => true,
        'qtd_vendida' => true,
        'desconto' => true,
        'created' => true,
        'modified' => true,
        'fruta' => true,
        'user' => true,
    ];
}
