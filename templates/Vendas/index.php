<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Venda> $vendas
 */
?>
<div class="vendas index content">
    <?= $this->Html->link(__('New Venda'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Vendas') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id_venda') ?></th>
                    <th><?= $this->Paginator->sort('fruta_id') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th><?= $this->Paginator->sort('qtd_vendida') ?></th>
                    <th><?= $this->Paginator->sort('desconto') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($vendas as $venda): ?>
                <tr>
                    <td><?= $this->Number->format($venda->id_venda) ?></td>
                    <td><?= $venda->has('fruta') ? $this->Html->link($venda->fruta->id_fruta, ['controller' => 'Frutas', 'action' => 'view', $venda->fruta->id_fruta]) : '' ?></td>
                    <td><?= $venda->has('user') ? $this->Html->link($venda->user->id_user, ['controller' => 'Users', 'action' => 'view', $venda->user->id_user]) : '' ?></td>
                    <td><?= $this->Number->format($venda->qtd_vendida) ?></td>
                    <td><?= $this->Number->format($venda->desconto) ?></td>
                    <td><?= h($venda->created) ?></td>
                    <td><?= h($venda->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $venda->id_venda]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $venda->id_venda]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $venda->id_venda], ['confirm' => __('Are you sure you want to delete # {0}?', $venda->id_venda)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
