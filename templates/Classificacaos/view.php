<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Classificacao $classificacao
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Classificacao'), ['action' => 'edit', $classificacao->id_classificacao], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Classificacao'), ['action' => 'delete', $classificacao->id_classificacao], ['confirm' => __('Are you sure you want to delete # {0}?', $classificacao->id_classificacao), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Classificacaos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Classificacao'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="classificacaos view content">
            <h3><?= h($classificacao->id_classificacao) ?></h3>
            <table>
                <tr>
                    <th><?= __('Classificacao') ?></th>
                    <td><?= h($classificacao->classificacao) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id Classificacao') ?></th>
                    <td><?= $this->Number->format($classificacao->id_classificacao) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($classificacao->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($classificacao->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Frutas') ?></h4>
                <?php if (!empty($classificacao->frutas)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id Fruta') ?></th>
                            <th><?= __('Classificacao Id') ?></th>
                            <th><?= __('Nome') ?></th>
                            <th><?= __('Fresca') ?></th>
                            <th><?= __('Qtd Disponivel') ?></th>
                            <th><?= __('Preco') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($classificacao->frutas as $frutas) : ?>
                        <tr>
                            <td><?= h($frutas->id_fruta) ?></td>
                            <td><?= h($frutas->classificacao_id) ?></td>
                            <td><?= h($frutas->nome) ?></td>
                            <td><?= h($frutas->fresca) ?></td>
                            <td><?= h($frutas->qtd_disponivel) ?></td>
                            <td><?= h($frutas->preco) ?></td>
                            <td><?= h($frutas->created) ?></td>
                            <td><?= h($frutas->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Frutas', 'action' => 'view', $frutas->id_fruta]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Frutas', 'action' => 'edit', $frutas->id_fruta]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Frutas', 'action' => 'delete', $frutas->id_fruta], ['confirm' => __('Are you sure you want to delete # {0}?', $frutas->id_fruta)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
