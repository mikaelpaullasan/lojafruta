<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Classificacao> $classificacaos
 */
?>
<div class="classificacaos index content">
    <?= $this->Html->link(__('New Classificacao'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Classificacaos') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id_classificacao') ?></th>
                    <th><?= $this->Paginator->sort('classificacao') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($classificacaos as $classificacao): ?>
                <tr>
                    <td><?= $this->Number->format($classificacao->id_classificacao) ?></td>
                    <td><?= h($classificacao->classificacao) ?></td>
                    <td><?= h($classificacao->created) ?></td>
                    <td><?= h($classificacao->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $classificacao->id_classificacao]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $classificacao->id_classificacao]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $classificacao->id_classificacao], ['confirm' => __('Are you sure you want to delete # {0}?', $classificacao->id_classificacao)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
