<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Fruta> $frutas
 */
?>
<div class="frutas index content">
    <?= $this->Html->link(__('New Fruta'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Frutas') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id_fruta') ?></th>
                    <th><?= $this->Paginator->sort('classificacao_id') ?></th>
                    <th><?= $this->Paginator->sort('nome') ?></th>
                    <th><?= $this->Paginator->sort('fresca') ?></th>
                    <th><?= $this->Paginator->sort('qtd_disponivel') ?></th>
                    <th><?= $this->Paginator->sort('preco') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($frutas as $fruta): ?>
                <tr>
                    <td><?= $this->Number->format($fruta->id_fruta) ?></td>
                    <td><?= $fruta->has('classificacao') ? $this->Html->link($fruta->classificacao->id_classificacao, ['controller' => 'Classificacaos', 'action' => 'view', $fruta->classificacao->id_classificacao]) : '' ?></td>
                    <td><?= h($fruta->nome) ?></td>
                    <td><?= h($fruta->fresca) ?></td>
                    <td><?= $this->Number->format($fruta->qtd_disponivel) ?></td>
                    <td><?= $this->Number->format($fruta->preco) ?></td>
                    <td><?= h($fruta->created) ?></td>
                    <td><?= h($fruta->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $fruta->id_fruta]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fruta->id_fruta]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fruta->id_fruta], ['confirm' => __('Are you sure you want to delete # {0}?', $fruta->id_fruta)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
