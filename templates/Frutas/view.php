<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fruta $fruta
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Fruta'), ['action' => 'edit', $fruta->id_fruta], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Fruta'), ['action' => 'delete', $fruta->id_fruta], ['confirm' => __('Are you sure you want to delete # {0}?', $fruta->id_fruta), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Frutas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Fruta'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="frutas view content">
            <h3><?= h($fruta->id_fruta) ?></h3>
            <table>
                <tr>
                    <th><?= __('Classificacao') ?></th>
                    <td><?= $fruta->has('classificacao') ? $this->Html->link($fruta->classificacao->id_classificacao, ['controller' => 'Classificacaos', 'action' => 'view', $fruta->classificacao->id_classificacao]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Nome') ?></th>
                    <td><?= h($fruta->nome) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id Fruta') ?></th>
                    <td><?= $this->Number->format($fruta->id_fruta) ?></td>
                </tr>
                <tr>
                    <th><?= __('Qtd Disponivel') ?></th>
                    <td><?= $this->Number->format($fruta->qtd_disponivel) ?></td>
                </tr>
                <tr>
                    <th><?= __('Preco') ?></th>
                    <td><?= $this->Number->format($fruta->preco) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($fruta->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($fruta->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Fresca') ?></th>
                    <td><?= $fruta->fresca ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Vendas') ?></h4>
                <?php if (!empty($fruta->vendas)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id Venda') ?></th>
                            <th><?= __('Fruta Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Qtd Vendida') ?></th>
                            <th><?= __('Desconto') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($fruta->vendas as $vendas) : ?>
                        <tr>
                            <td><?= h($vendas->id_venda) ?></td>
                            <td><?= h($vendas->fruta_id) ?></td>
                            <td><?= h($vendas->user_id) ?></td>
                            <td><?= h($vendas->qtd_vendida) ?></td>
                            <td><?= h($vendas->desconto) ?></td>
                            <td><?= h($vendas->created) ?></td>
                            <td><?= h($vendas->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Vendas', 'action' => 'view', $vendas->id_venda]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Vendas', 'action' => 'edit', $vendas->id_venda]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Vendas', 'action' => 'delete', $vendas->id_venda], ['confirm' => __('Are you sure you want to delete # {0}?', $vendas->id_venda)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
